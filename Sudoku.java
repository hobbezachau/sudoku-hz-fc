package sudoku;


public class Sudoku implements SudokusolverInterface {

	private int[][] board;

	public Sudoku() {
		board = new int [9][9];

	}
	@Override
	
	public boolean solve() {
		return solve(0, 0);
	}
	
	private boolean solve(int row, int col) {
		if(row == 9) {
			return true;
		}else if( col == 9) {
			return solve(row+1, 0);
		}else {
			if(board[row][col] != 0 ) {
				if( isValid() && solve(row, col+1)) {
					return true; 
				}else {
					return false; 
				}
			}else {
				for(int i = 1; i<10; i++) {
					add(row, col, i);
					if( isValid() && solve(row, col+1)) {
						return true; 
					}else {
						remove(row, col);
					}
				}
			}
		}
		return false;
		
	}
			
	

	@Override
	public void add(int row, int col, int digit) {
		if( digit > 9 ) {
			throw new IllegalArgumentException(" Värdet är för stort");
		}else if( digit < 0 ) {
			throw new IllegalArgumentException(" Värdet är för litet");
		}else {
			board [row][col] = digit;
		}
		// TODO Auto-generated method stub

	}

	@Override
	public void remove(int row, int col) {
		board [row][col] = 0; 
		// TODO Auto-generated method stub
		
	}

	@Override
	public int get(int row, int col) {
		// TODO Auto-generated method stub
		return board [row][col];
	}

	@Override
	public boolean isValid() {
		// TODO Auto-generated method stub
		for(int i = 0; i < board.length; i++) { 
			for (int j = 0; j < board[i].length; j++) {
				int k = 1;
				int p = 1; 
				while(j+k < 9) {
					if(board[i][j] != 0 && board [i][j] == board [i][j+k]) {
						return false; 
					}
					k++;
				}
				
				while(i+p < 9) {
					if(board[i][j] != 0 && board [i][j] == board [i+p][j]) {
						return false; 
					
					}
					p++; 
				}
				
				int x = i/3;
				int y = j/3; 
				for(int a = x*3; a < (x+1)*3 ; a++) {
					for(int b = y*3; b < (y+1)*3 ; b++)
					if(board[i][j] == board[a][b] && a != i && j != b && board[i][j] != 0){// && ((x+a) != i && (y+b) !=j)) {
						return false;
					}
				} 
						
			}
				
		  }return true;
	   }
		
		
	

	@Override
	public void clear() {
		for(int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				remove(i,j); 
			}
		}
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setMatrix(int[][] m) {
		if(m.length != board.length || m[0].length != board[0].length) {
			throw new IllegalArgumentException("Matrisernas storlekar matchar inte");
		}else {
		for(int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				add(i ,j, m[i][j]);
			}
		}
		}
		// TODO Auto-generated method stub
		
	}

	@Override
	public int[][] getMatrix() {
		// TODO Auto-generated method stub
		return board;
	}

}
