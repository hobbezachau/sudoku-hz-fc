package sudoku;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class  SudokuGraphics {
	
	private JTextField [][] grid = new  JTextField[9][9];
	
    public SudokuGraphics(Sudoku s) {
        SwingUtilities.invokeLater(() -> createWindow(s, "Sudoku", 500, 500));      
}
    private void createWindow(Sudoku s, String title, int width, int height) {
    	//JTextField grid[][] = new JTextField[9][9];
    	
    	
    	JPanel panel1 = new JPanel();
    	JPanel gridPanel   = new JPanel(new GridLayout(3, 3));
    

        JPanel minisquarePanels[][] = new JPanel[3][3];
        	
        JFrame frame = new JFrame(title);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container pane = frame.getContentPane();
        frame.setPreferredSize(new Dimension(500, 500));
        
        Font FONT = new Font("Times ", 
                Font.CENTER_BASELINE, 
                25);
        
        for(int i = 0; i < 9; i++) {
        	for(int j = 0; j < 9; j++) {
        		
        		JTextField field = new JTextField();
        		
        		field.setFont(FONT);
        
        		field.setPreferredSize(new Dimension(25, 25));
        		if(s.getMatrix()[i][j] == 0) {
        			field.setText("");
        		}
        		 if ( s.getMatrix()[i][j] != 0) {
        			field.setText(String.valueOf(s.get(i, j)));
        		}
        		
        		else if(i < 3 && j < 3 || (i < 3 && j > 5) ||  (i > 2 && i < 6 &&  j > 2 && j < 6) ||  ( i > 5 && (j < 3 || j > 5)) )    {
                    field.setBackground(Color.green);
        		}
        		grid[i][j] = field;
        	}
        }
        
      
        
        for (int y = 0; y < 3; ++y) {
            for (int x = 0; x < 3; ++x) {
                JPanel panel = new JPanel();
                panel.setLayout(new GridLayout(3, 3));
                minisquarePanels[y][x] = panel;
                gridPanel.add(panel);
            }
        }
        
        for (int y = 0; y < 9; ++y) {
            for (int x = 0; x < 9; ++x) {
                int minisquareX = x/3;
                int minisquareY = y/3;

                minisquarePanels[minisquareY][minisquareX].add(grid[y][x]);
            }
        }

        JButton b1 = new JButton("Solve");
        JButton b2 = new JButton("Reset");
        
        ButtonGroup group = new ButtonGroup();
        
        b1.addActionListener(event -> {
        	if(checkBoard(s, pane)){
        		updateboard(s);
        		if(!s.isValid()) {
        			JOptionPane.showMessageDialog(pane, "Finns inte");
        		}else {
        			s.solve();
        			buildgrid(s);
        		}
        	}
        });
        
        b2.addActionListener(event -> {
        	s.clear();
        	
        	buildgrid(s);
        	updateboard(s);
        	//grid = String.valueOf(s.getMatrix()[][]));
        });
        
        group.add(b1);
        group.add(b2);
        
        panel1.add(b1);
        panel1.add(b2);
        

        pane.add(panel1, BorderLayout.SOUTH);
        pane.add(gridPanel);
       

     frame.pack();
     frame.setVisible(true);
 }


public void buildgrid(Sudoku s) {
	//Sudoku ss = new Sudoku();
	//s.clear();
	for(int i =0; i < 9; i++) {
		for(int j = 0; j <9; j++ ) {
			if(s.get(i, j) == 0) {
				grid[i][j].setText("");
			}else {
			grid [i][j].setText(String.valueOf(s.get(i,j)));
			}
		}				
	}
	
	
}

public void updateboard(Sudoku s) {
	//Sudoku ss = new Sudoku();
	//s.clear();
	for(int i =0; i < 9; i++) {
		for(int j = 0; j <9; j++ ) {
			if(grid[i][j].getText().equals("")) {
			s.add(i, j, 0);
			}else {
				s.add(i, j, Integer.parseInt(grid [i][j].getText()));
		}
	 }
	}
}
public boolean checkBoard(Sudoku s, Container pane) {
	for(int i = 0; i < 9; i++) {
		for(int j = 0; j <9; j++) {
			if(!grid[i][j].getText().equals("")) {
				try {
					int v = Integer.parseInt(grid[i][j].getText());
				} catch (NumberFormatException e) {
					JOptionPane.showMessageDialog(pane, "Vänligen skriv endast tal mellan 1-9");
					return false;
				}
				if(Integer.parseInt(grid[i][j].getText()) < 1 || Integer.parseInt(grid[i][j].getText()) > 9) {
					JOptionPane.showMessageDialog(pane, "Vänligen skriv endast tal mellan 1-9");
					return false;
				}
			} 
		}
	}
	return true;
}

}