package sudoku;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class STester {
	private SudokusolverInterface sudoku;

	@BeforeEach
	void setUp() throws Exception {
		this.sudoku = new Sudoku();
	}

	@AfterEach
	void tearDown() throws Exception {
		sudoku = null;
	}
	
	
	@Test
	void getNumber() {
		assertEquals(sudoku.get(0, 0), 0, "Did not return the right number");
	}
	
	@Test
	void add() {
		sudoku.add(1,1,1);
		assertEquals(sudoku.get(1, 1), 1, "Was not able to set the number in chosen box");
	}
	
	
	@Test
	void remove() {
		sudoku.add(1,1,1);
		sudoku.remove(1,1);
		assertEquals(sudoku.get(1, 1), 0, "Was not able remove the number in chosen box");

	}
	@Test
	void isValidSameRow(){
		sudoku.add(0, 2, 8);
		sudoku.add(0, 5, 8);
		assertFalse(sudoku.isValid(), "Did not recognize two of the same number in the same row");
	}
	
	@Test
	void isValidSameCol(){
		sudoku.add(0, 2, 8);
		sudoku.add(2, 2, 8);
		assertFalse(sudoku.isValid(), "Did not recognize two of the same number in the same row");
	}

	
	@Test
	void testSolvableBoard() {
		sudoku.add(0, 2, 8);
		sudoku.add(0, 5, 9);
		sudoku.add(0, 7, 6);
		sudoku.add(0, 8, 2);
		sudoku.add(1, 8, 5);
		sudoku.add(2, 0, 1);
		sudoku.add(2, 2, 2);
		sudoku.add(2, 3, 5);
		sudoku.add(3, 3, 2);
		sudoku.add(3, 4, 1);
		sudoku.add(3, 7, 9);
		sudoku.add(4, 1, 5);
		sudoku.add(4, 6, 6);
		sudoku.add(5, 0, 6);
		sudoku.add(5, 7, 2);
		sudoku.add(5, 8, 8);
		sudoku.add(6, 0, 4);
		sudoku.add(6, 1, 1);
		sudoku.add(6, 3, 6);
		sudoku.add(6, 5, 8);
		sudoku.add(7, 0, 8);
		sudoku.add(7, 1, 6);
		sudoku.add(7, 4, 3);
		sudoku.add(7, 6, 1);
		sudoku.add(8, 6, 4);
		sudoku.solve();
		assertTrue(sudoku.isValid(), "Could not solve solvable sudoku");
	}
	
	
	@Test
	void solveEmpty() {
		sudoku.solve();
		assertTrue(sudoku.isValid(), "Could not solve empty sudoku");
	}
	
	@Test 
	void unsolvable() {
		sudoku.add(0, 2, 8);
		sudoku.add(1, 5, 8);
		sudoku.add(2, 7, 6);
		sudoku.add(2, 8, 5);
		sudoku.add(2, 6, 4);
		sudoku.add(2, 0, 1);
		sudoku.add(2, 2, 2);
		sudoku.add(2, 3, 5);
		sudoku.add(3, 3, 2);
		sudoku.add(3, 4, 1);
		sudoku.add(3, 7, 9);
		sudoku.add(4, 1, 5);
		sudoku.add(4, 6, 6);
		sudoku.add(5, 0, 6);
		sudoku.add(5, 7, 2);
		sudoku.add(5, 8, 8);
		sudoku.add(6, 0, 4);
		sudoku.add(6, 1, 1);
		assertFalse(sudoku.solve(), "Solved an unsolvable sudoku");
	}
	
	@Test
	void isValid() {
		sudoku.add(1, 1, 9);
		sudoku.add(4, 4, 9);
		sudoku.add(8, 4, 5);
		assertTrue(sudoku.isValid(),"Did not recognize that all the numbers were valid");
	}
	

	@Test
	void sameRow() {
		sudoku.add(0, 0, 8);
		sudoku.add(0, 5, 8);
		assertFalse(sudoku.solve(), "Did not recognize two same digits at the same row");
	}
	
	@Test
	void testSameCol() {
		sudoku.add(0, 0, 8);
		sudoku.add(4, 0, 8);
		assertFalse(sudoku.solve(), "Did not recognize two same digits at the same column");
	}
	
	@Test
	void clear() {
		for(int r = 0; r<9; r++) {
			for(int c = 0; c<9; c++) {
				sudoku.add(r, c, 2);
			}
		}
		
		sudoku.clear();
		for(int r = 0; r<9; r++) {
			for(int c = 0; c<9; c++) {
				assertEquals(sudoku.get(r, c), 0,"Did not manage to clear all numbers");
			}
		}
	}
	
	@Test 
	void testSetMatrix() {
		int [][] m = new int [9][9];
		m[1][1] = 1;
		m[2][2] = 2;
		m[3][3] = 3;
		m[4][4] = 4;
		sudoku.setMatrix(m);
		
		for(int i=0;i<9;i++) {
			for(int k=0; k<9; k++) {
				assertEquals(sudoku.get(i, k), m[i][k], "Did not successfully set the sudoku with setMatrix");
			}
		}
	}
	
	
	
	@Test
	void testGetMatrix() {
		sudoku.add(1,1,1);
		sudoku.add(2,2,2);
		sudoku.add(3,3,3);
		sudoku.add(4,4,4);
		
		int[][] m = sudoku.getMatrix();
		for(int i=0;i<9;i++) {
			for(int k=0; k<9; k++) {
				assertEquals(sudoku.get(i, k), m[i][k], "Did not successfully get the sudoku with getMatrix");
			}
		}
	}
	
	
	
}
